#pragma once
#include "GameObject.h"

namespace Games
{
	class Token : virtual public GameObject
	{
	public:
		Token(const str name, Player* owner);
		~Token();
	};
}
