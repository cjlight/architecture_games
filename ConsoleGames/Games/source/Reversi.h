#pragma once
#include "BoardGame.h"
#include "Player.h"
#include "Token.h"

namespace Games
{
	class Reversi :
		virtual public BoardGame
	{
	public:
		// Player indices 
		enum PlayerIndex
		{
			ONE = 0, TWO, COUNT
		};

		Reversi()
		{};
		Reversi(Player* player1, Player* player2);
		~Reversi();

		// Inherited via Game
		virtual bool ini() override;
		virtual bool update() override;
		virtual bool free() override;

		virtual Player* getWinner() override;

	protected:

	};
}

