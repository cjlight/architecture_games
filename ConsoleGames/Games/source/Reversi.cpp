#include "Reversi.h"
#include <vector>
#include "Token.h"
#include "BoardPlayer.h"

namespace Games
{
	Reversi::Reversi(Player* player1, Player* player2) :
		Game("Reversi", vector<Player*>{player1, player2}),
		BoardGame(8, 8)
	{}

	Reversi::~Reversi()
	{}

	bool Reversi::ini()
	{
		BoardGame::ini();

		return true;
	}

	bool Reversi::update()
	{
		BoardGame::update();
		
		// TODO: calculate end game

		return true;
	}

	bool Reversi::free()
	{
		return true;
	}

	Player* Reversi::getWinner()
	{
		// TODO: return actual winner
		return mPlayers[0];
	}

}