#pragma once

	namespace Presentations
	{
		class Presentation
		{
		public:
			Presentation();
			~Presentation();

			// True if presenting
		 virtual bool present() = 0;

		 // True if presenting
		 virtual bool update() = 0;
		};
	}