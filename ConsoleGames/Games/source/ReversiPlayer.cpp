#include "ReversiPlayer.h"
#include "Token.h"

namespace Games
{

	void ReversiPlayer::ini()
	{
		switch (mTurnOrder)
		{
		case 0:
		assignObject(new Token("White Stone", this));
		placePiece(4, 4);
		placePiece(5, 5);
		break;
		case 1:
		assignObject(new Token("Black Stone", this));
		placePiece(4, 5);
		placePiece(5, 4);
		break;
		default:
		throw std::logic_error("mTurnOrder does not support two players");
		break;
		}
	}

	void ReversiPlayer::placePiece(us x, us y)
	{
		getBoardGame()->placeObject(x, y, mObjects[0]);
	}
}