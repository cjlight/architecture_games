#include "ConsoleReversi.h"

namespace ConsoleGames
{
	ConsoleReversi::ConsoleReversi(Player* player1, Player* player2) : 
		Game("Reversi", vector<Player*>{player1, player2}), BoardGame(8, 8)
	{
	}
	
	ConsoleReversi::~ConsoleReversi()
	{}

	bool ConsoleReversi::update()
	{
		Reversi::update();
		ConsoleBoardGame::present();

		// HACK: remove
		cout << mBoard->getString() << endl;
		this->setPlaying(false);

		return false;
	}
}
