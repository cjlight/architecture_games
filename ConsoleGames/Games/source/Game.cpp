#include "Game.h"

namespace Games
{
	Game::Game(const str name, vector<Player*> players)
	{
		mName = name;
		mIsPlaying = true;
		mPlayers = players;
	}

	Game::~Game()
	{
		free();
	}

	Player* Game::execute()
	{
		ini();

		while (mIsPlaying)
		{
			update();
		}

		return getWinner();
	}

	bool Game::free()
	{
		return true;
	}

	Player* Game::getWinner()
	{
		return NULL;
	}

	const str Game::getName()
	{
		return mName;
	}

	void Game::setName(const str name)
	{
		mName = name;
	}

	const bool Game::isPlaying()
	{
		return mIsPlaying;
	}

	void Game::setPlaying(const bool flag)
	{
		mIsPlaying = flag;
	}
}