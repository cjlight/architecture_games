#pragma once
#include "BoardPlayer.h"
#include "ReversiPlayer.h"

namespace Games
{
	class ReversiAI :
		public ReversiPlayer
	{
	public:
		ReversiAI(const str name, const us turnOrder) : Player(name), ReversiPlayer(name, turnOrder)
		{};

		virtual void update() override;
	};
}