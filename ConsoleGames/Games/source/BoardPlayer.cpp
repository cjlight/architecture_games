#include "BoardPlayer.h"

namespace Games
{
	BoardPlayer::BoardPlayer(const str name, const us turnOrder) :
		Player(name), mTurnOrder(turnOrder)
	{}

	BoardGame* BoardPlayer::getBoardGame() const
	{
		return dynamic_cast<BoardGame*>(mGame);
	}
	us BoardPlayer::getTurnOrder() const
	{
		return mTurnOrder;
	}
	void BoardPlayer::setTurnOrder(Games::us val)
	{
		mTurnOrder = val;
	}
}