#pragma once
#include "BoardPlayer.h"

namespace Games
{
	class ReversiPlayer :
		public BoardPlayer
	{
	public:
		ReversiPlayer(const str name, const us turnOrder) : Player(name), BoardPlayer(name, turnOrder)
		{};

		virtual void ini() override;

		// Place a reversi stone
		void placePiece(us x, us y);
	};
}
