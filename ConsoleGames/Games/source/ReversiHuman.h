#pragma once
#include "ReversiPlayer.h"

namespace Games
{
	class ReversiHuman :
		public ReversiPlayer
	{
	public:
		ReversiHuman(const str name, const us turnOrder) : Player(name), ReversiPlayer(name, turnOrder)
		{};

		virtual void update() override;
	};
}
