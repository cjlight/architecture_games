#pragma once
#include "Games.h"
#include "Player.h"

namespace Games
{
	class Player;

	class GameObject
	{
	public:
		GameObject(const str name, Player* owner);
		~GameObject();

		// Get/Set name
		void setName(const str name);
		const str getName();

		// Get/Set Owner
		Player* getOwner() const;
		void setOwner(Player* val);

	protected:
		// Game object name
		str mName;

		// The owner of this object
		Player* mOwner;
	};
}
