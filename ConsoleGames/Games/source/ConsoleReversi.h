#pragma once
#include "Games.h"
#include "Reversi.h"
#include "ConsoleBoardGame.h"

namespace ConsoleGames
{
	using namespace Presentations;
	using namespace Games;

		class ConsoleReversi :
			public Reversi, public ConsoleBoardGame
		{
		public:
			ConsoleReversi(Player* player1, Player* player2);
			~ConsoleReversi();

			// Update the game
			bool update();
		};
}

