#pragma once
#include "Player.h"
#include "Board.h"
#include "BoardGame.h"

namespace Games
{
	class BoardPlayer :
		virtual public Player
	{
	public:
		BoardPlayer(const str name, const us turnOrder);

		// Get the board game the player is assigned to
		BoardGame* getBoardGame() const;

		// Get/Set TurnOrder
		us getTurnOrder() const;
		void setTurnOrder(Games::us val);

	protected:
		// The turn order for the player
		us mTurnOrder;
	};
}
